import sys
import numpy as np
import pandas as pd
from enum import Enum

def getselfattr(name):
    return getattr(sys.modules[__name__], name)

def export_enum_constants(enum):
    for ec in enum:
        setattr(sys.modules[__name__], ec.name, ec)
    return enum

DEFAULTS = {
    "mode": "model",
    "train_fraction": 0.5,
    "train_period": None,
    "test_period": None,
    "prev_cols": "",
    "days": 2,
    "normalizer": "cumzscore",
    "diffs": False,
    "net_arch": "ffn",
    "net_levels": 2,
    "net_size": 32,
    "net_dropout": 0,
    "optimizer": "SGD",
    "learn_rate": 0.01,
    "learn_decay": 0.0,
    "learn_momentum": 0.5,
    "batch_size": 1,
    "epochs": 50,
    "save_model": None,
    "load_model": None,
    "tuning": False,
    "capital": 50000,
    "test_on_train": False,
    "buy_prob": 0.5,
    "buy_prob_auto": False,
    "oracle_prob": 1.0,
    "seed": None
}

@export_enum_constants
class Decision(Enum):
    SELL = 0
    BUY = 1

DATE = "DATE"
OPEN = "OPEN"
CLOSE = "CLOSE"

def clean_dataset(dataset, date_col="Date", open_col="Open",
        close_col="Close", added_prev_cols=[]):
    """Extract key columns from raw data and assign standard names."""
    cols = {
        DATE: dataset[date_col],
        OPEN: dataset[open_col],
        CLOSE: dataset[close_col]
    }
    for col in added_prev_cols:
        cols[col + "_prev"] = dataset[col].shift(1)
    return pd.DataFrame(cols).set_index(DATE)

def normalize_meanone(dataset):
    return dataset / dataset.values.mean()

def normalize_zscore(dataset):
    return (dataset - dataset.values.mean()) / dataset.values.std(ddof=1)

def normalize_cumzscore(dataset):
    rolling = dataset.rolling(len(dataset), min_periods=1)
    return (dataset - rolling.mean()) / rolling.std()

def netarch_ffn(config, inputs):
    from keras.models import Sequential
    from keras.layers import Flatten, Dense, Dropout
    nn = Sequential()
    nn.add(Flatten(input_shape=(config["days"], inputs)))
    nn.add(Dense(units=config["net_size"], activation="relu",
                 kernel_initializer='random_uniform', bias_initializer='zeros'))
    if config["net_dropout"]:
        nn.add(Dropout(config["net_dropout"]))
    for i in range(1, config["net_levels"]):
        nn.add(Dense(units=config["net_size"]//(2**i), activation='relu',
                     kernel_initializer='random_uniform', bias_initializer='zeros'))
        if config["net_dropout"]:
            nn.add(Dropout(config["net_dropout"]))
    nn.add(Dense(units=1))
    return nn

def netarch_lstm(config, inputs):
    from keras.models import Sequential
    from keras.layers import Dense, LSTM, Dropout
    nn = Sequential()
    nn.add(LSTM(units=config["net_size"], input_shape=(config["days"], inputs),
                activation="relu", return_sequences=config["net_levels"]>1))
    if config["net_dropout"]:
        nn.add(Dropout(config["net_dropout"]))
    for i in range(1, config["net_levels"]):
        nn.add(LSTM(units=config["net_size"]//(2**i), activation="relu",
                         return_sequences=i!=config["net_levels"]-1))
        if config["net_dropout"]:
            nn.add(Dropout(config["net_dropout"]))
    nn.add(Dense(units=1))
    return nn

def netarch_lstmautoenc(config, inputs):
    from keras.models import Model
    from keras.layers import (Input, Dense, LSTM, Dropout, RepeatVector,
                              TimeDistributed)
    visible = Input(shape=(config["days"], inputs))
    encoder = visible
    for i in range(config["net_levels"]):
        encoder = LSTM(config["net_size"]//(2**i), activation="relu",
                       return_sequences=(i!=config["net_levels"]-1))(encoder)
        if config["net_dropout"]:
            encoder = Dropout(config["net_dropout"])(encoder)
    decoders_size = config["net_size"]//(2**config["net_levels"])
    decoder = RepeatVector(config["days"])(encoder)
    decoder = LSTM(decoders_size, activation="relu",
                   return_sequences=True)(decoder)
    decoder = TimeDistributed(Dense(inputs))(decoder)
    predicter = RepeatVector(config["days"])(encoder)
    predicter = LSTM(decoders_size, activation="relu")(predicter)
    predicter = Dense(1)(predicter)
    model = Model(inputs=visible, outputs=[decoder, predicter])
    return model

def select_valid(X, Y):
    return ~np.isnan(X).any((1, 2)) & ~np.isnan(Y).any(1)

class NeuralModel:
    def __init__(self, config, added_inputs=[]):
        self.config = config
        self.added_inputs = added_inputs
    def prepare_data(self, dataset, slice):
        if self.config["diffs"]:
            dataset = dataset.diff()
        X = np.stack((dataset[[OPEN] + self.added_inputs].shift(i).iloc[slice]
                     for i in range(self.config["days"]-1, -1, -1)), axis=1)
        Y = dataset[CLOSE].iloc[slice].values.reshape(-1, 1)
        return X, Y
    def configure(self):
        from keras import optimizers
        self.nn = getselfattr("netarch_" + self.config["net_arch"]) \
                (self.config, 1 + len(self.added_inputs))
        if self.config["optimizer"] == "SGD":
            optimizer = optimizers.SGD(lr=self.config["learn_rate"],
                    momentum=self.config["learn_momentum"],
                    decay=self.config["learn_decay"], nesterov=False)
        else:
            optimizer = getattr(optimizers, self.config["optimizer"]) \
                (lr=self.config["learn_rate"], decay=self.config["learn_decay"])
        self.nn.compile(loss='mean_squared_error', optimizer=optimizer)
    def _fit(self, X, Y, silent=False, **kwargs):
        if self.config["net_arch"] == "lstmautoenc":
            Y = [X, Y]
        self.nn.fit(X, Y, verbose=0 if silent else 1, **kwargs)
    def fit(self, X, Y, epochs, silent=False):
        valid = select_valid(X, Y)
        input, target = X[valid], Y[valid]
        self._fit(input, target, silent, batch_size=self.config["batch_size"],
                    epochs=epochs)
    def save(self, filename):
        self.nn.save(filename)
    def load(self, filename):
        from keras.models import load_model
        self.nn = load_model(filename)
    def _predict(self, X):
        preds = self.nn.predict(X)
        if self.config["net_arch"] == "lstmautoenc":
            preds = preds[1]
        return preds
    def predict(self, X, Y):
        if self.config["tuning"]:
            valid = select_valid(X, Y)
            preds = np.full((X.shape[0], 1), np.nan)
            for i in range(X.shape[0]):
                if not valid[i]:
                    continue
                preds[i] = self._predict(X[np.newaxis, i])
                self._fit(X[np.newaxis, i], Y[np.newaxis, i], True,
                          batch_size=1, epochs=1)
            return preds
        else:
            return self._predict(X)
    def decide(self, X, Y):
        decisions = np.repeat(None, X.shape[0])
        valid = select_valid(X, Y)
        buy = (Y[valid, 0] >= 0 if self.config["diffs"]
               else Y[valid, 0] >= X[valid, -1, 0])
        decisions[valid] = np.where(buy, BUY, SELL)
        return decisions

@export_enum_constants
class BuyMode(Enum):
    SINGLE = 0   # don't keep more than one stock at a time
    MULTI = 1    # keep more stocks buying one at a time
    BULK = 2     # immediately buy as many stocks as possible

class StandardTrader: # FIXME check capital availability
    def __init__(self, capital=0, day_trade=False, buy_mode=SINGLE, bulk_sell=False):
        self.day_trade = day_trade
        self.buy_mode = buy_mode
        self.bulk_sell = bulk_sell
        self.capital = capital
        self.stocks = 0
    def trade(self, open_price, close_price, decision, last_day=False):
        if decision is BUY:
            if not self.stocks or self.buy_mode is not SINGLE:
                to_buy = self.capital // open_price
                if to_buy:
                    if self.buy_mode is not BULK:
                        to_buy = 1
                    self.capital -= to_buy * open_price
                    self.stocks += to_buy
            if self.day_trade and self.capital >= open_price:
                self.capital += close_price - open_price
        if (decision is SELL or last_day) and self.stocks:
            to_sell = self.stocks if self.bulk_sell else 1
            self.capital += to_sell * open_price
            self.stocks -= to_sell

def trade(prices, all_traders, decisions):
    names, traders = zip(*all_traders.items())
    capital_trace = []
    for day in range(len(prices)):
        open_price = prices[OPEN][day]
        close_price = prices[CLOSE][day]
        last_day = day == len(prices) - 1
        if decisions[day] is not None:
            for trader in traders:
                trader.trade(open_price, close_price, decisions[day], last_day)
        capital_trace.append([trader.capital for trader in traders])
    capital_trace = pd.DataFrame(capital_trace, index=prices.index, columns=names)
    return capital_trace

def trade_simulation(config, model, dataset, slice, X, Y):
    if config["mode"] == "model":
        preds = model.predict(X, Y)
        decisions = model.decide(X, preds)
    elif config["mode"] == "random":
        buy_prob = config["buy_prob"]
        decisions = np.random.choice([SELL, BUY], slice.stop - slice.start,
                                     p=[1-buy_prob, buy_prob])
    elif config["mode"] == "oracle":
        right_decisions = model.decide(X, Y)
        wrong_decisions = np.where(right_decisions == SELL, BUY, SELL)
        num_correct = int(config["oracle_prob"] * len(right_decisions))
        correct = np.concatenate([np.repeat(True, num_correct),
                                  np.repeat(False, len(right_decisions)-num_correct)])
        np.random.shuffle(correct)
        decisions = np.where(correct, right_decisions, wrong_decisions)
    else:
        raise Exception("unknown mode: {}".format(config["mode"]))
    right_decisions = model.decide(X, Y)
    # trade
    trade_prices = dataset.iloc[slice]
    #trader = SingleStockTrader(50000, config["day_trade"])
    traders = {
        "simple": StandardTrader(config["capital"]),
        "simpledt": StandardTrader(config["capital"], day_trade=True),
        "multi": StandardTrader(config["capital"], buy_mode=MULTI),
        "multidt": StandardTrader(config["capital"], day_trade=True, buy_mode=MULTI),
        "bulksell": StandardTrader(config["capital"], buy_mode=MULTI, bulk_sell=True),
        "bulkselldt": StandardTrader(config["capital"], day_trade=True, buy_mode=MULTI, bulk_sell=True),
        "bulkbuy": StandardTrader(config["capital"], buy_mode=BULK),
        "bulktrade": StandardTrader(config["capital"], buy_mode=BULK, bulk_sell=True)
    }
    capital_trace = trade(trade_prices, traders, decisions)
    results = {"capital_{}".format(name): trader.capital
               for name, trader in traders.items()}
    results["accuracy"] = (decisions[decisions != None]
                           == right_decisions[decisions != None]).mean()
    return results, capital_trace

def set_seed(seed):
    import random
    import tensorflow as tf
    np.random.seed(seed)
    random.seed(seed)
    tf.set_random_seed(seed)

def configure_tensorflow(cpu_sequential=False, gpu_disable=False,
                         gpu_memory=None, gpu_growth=None):
    import tensorflow as tf
    from keras.backend.tensorflow_backend import set_session
    config = tf.ConfigProto()
    if cpu_sequential:
        config.intra_op_parallelism_threads = 1
        config.inter_op_parallelism_threads = 1
    if gpu_disable:
        config.device_count["GPU"] = 0
    if gpu_memory is not None:
        config.gpu_options.per_process_gpu_memory_fraction = gpu_memory
    if gpu_growth is not None:
        config.gpu_options.allow_memory_growth = gpu_growth
    set_session(tf.Session(config=config))

def run(silent=False, **kwargs):
    # merge configured values with defaults
    config = DEFAULTS.copy()
    config.update(kwargs)
    # backwards compatibility
    if "recurrent" in config:
        config["net_arch"] = "lstm" if config["recurrent"] else "ffn"
    # load stock data and extract needed columns (date, open, close)
    raw_dataset = pd.read_csv(config["data_file"])
    prev_cols = list(filter(None, config["prev_cols"].split(",")))
    dataset = clean_dataset(raw_dataset, added_prev_cols=prev_cols)
    # apply normalization
    normalized = getselfattr("normalize_" + config["normalizer"])(dataset)
    # define train and test periods
    if config["train_period"] and config["test_period"]:
        train_slice = slice(*dataset.index.slice_locs(*config["train_period"].split(":")))
        test_slice = slice(*dataset.index.slice_locs(*config["test_period"].split(":")))
    else:
        split_index = int(config["train_fraction"] * len(dataset))
        train_slice = slice(0, split_index)
        test_slice = slice(split_index, len(normalized))
    # extract model inputs and outputs
    model = NeuralModel(config, added_inputs=[c+"_prev" for c in prev_cols])
    train_X, train_Y = model.prepare_data(normalized, train_slice)
    test_X, test_Y = model.prepare_data(normalized, test_slice)
    if config["seed"]:
        set_seed(config["seed"])
    if config["mode"] == "model":
        if config["load_model"]:
            model.load(config["load_model"])
        else:
            model.configure()
            model.fit(train_X, train_Y, config["epochs"], silent)
            if config["save_model"]:
                model.save(config["save_model"])
    if config["mode"] == "random" and config["buy_prob_auto"]:
        decisions = model.decide(train_X, train_Y)
        config["buy_prob"] = (decisions == BUY).mean()
    results, capital_trace = trade_simulation(config, model, dataset,
            test_slice, test_X, test_Y)
    if config["test_on_train"]:
        train_results, _ = trade_simulation(config, model, dataset,
                train_slice, train_X, train_Y)
        for key, val in train_results.items():
            results["train_" + key] = val
    if config["mode"] == "random" and config["buy_prob_auto"]:
        results["buy_probability"] = config.pop("buy_prob")
    return results, capital_trace

def main(*args):
    from argparse import ArgumentParser
    parser = ArgumentParser()
    group = parser.add_argument_group("data loading")
    group.add_argument("-i", "--data-file", required=True, metavar="FILE",
            help="name of CSV file with daily stock values")
    group.add_argument("--train-fraction", type=float, default=DEFAULTS["train_fraction"],
            help="fraction of data to use as training set")
    group.add_argument("--train-period", type=str, default=DEFAULTS["train_period"],
            help="manually set training period as 'inclusive_start:exclusive_end'")
    group.add_argument("--test-period", type=str, default=DEFAULTS["test_period"],
            help="manually set trading period as 'inclusive_start:exclusive_end'")
    group = parser.add_argument_group("net input hyperparameters")
    group.add_argument("--prev-cols", type=str, default=DEFAULTS["prev_cols"],
            help="comma-separated names of columns whose previous day value "
                 "is used as net input in addition to current day open value")
    group.add_argument("-d", "--days", type=int, default=DEFAULTS["days"],
            help="number of previous days used for predictions")
    group.add_argument("-n", "--normalizer", type=str, default=DEFAULTS["normalizer"],
            help="data normalization method")
    group.add_argument("-g", "--diffs", action="store_true",
            help="train network on daily variations instead of actual values")
    group = parser.add_argument_group("net structure hyperparameters")
    group.add_argument("-a", "--net-arch", type=str, default=DEFAULTS["net_arch"],
            help="neural net architecture to use")
    group.add_argument("-y", "--net-levels", type=int, default=DEFAULTS["net_levels"],
            help="number of hidden layers in neural net")
    group.add_argument("-s", "--net-size", type=int, default=DEFAULTS["net_size"],
            help="number of nodes in the first hidden layer of neural net")
    group.add_argument("--net-dropout", type=float, default=DEFAULTS["net_dropout"],
            help="dropout rate applied to each hidden layer during training")
    group = parser.add_argument_group("net training hyperparameters")
    group.add_argument("--optimizer", type=str, default=DEFAULTS["optimizer"],
            help="name of Keras optimizer class to be used")
    group.add_argument("-l", "--learn-rate", type=float, default=DEFAULTS["learn_rate"],
            help="base learning rate for net training")
    group.add_argument("--learn-decay", type=float, default=DEFAULTS["learn_decay"],
            help="decay of learning rate")
    group.add_argument("--learn-momentum", type=float, default=DEFAULTS["learn_momentum"],
            help="learning momentum (SGD optimizer only)")
    group.add_argument("-b", "--batch-size", type=int, default=DEFAULTS["batch_size"],
            help="size of network train batches")
    group.add_argument("-e", "--epochs", type=int, default=DEFAULTS["epochs"],
            help="number of model training epochs")
    group.add_argument("-t", "--tuning", action="store_true",
            help="continue to train the model while trading")
    group = parser.add_argument_group("miscellaneous options")
    group.add_argument("--load-model", metavar="FILE", default=None,
            help="name of model file to be loaded (no training happens)")
    group.add_argument("--save-model", metavar="FILE", default=None,
            help="name of model file to be saved after training")
    group.add_argument("-r", "--seed", type=int,
            help="randomness seed (for initial net weights or random trading)")
    group.add_argument("-c", "--capital", type=float, default=DEFAULTS["capital"],
            help="amount of initial capital available to traders")
    group.add_argument("--test-on-train", action="store_true",
            help="also simulate trading on training period")
    group = parser.add_argument_group("baseline modes (no model trained)")
    group.add_argument("--random", dest="mode", action="store_const", const="random",
            help="trade with random decisions instead of using model")
    group.add_argument("--buy-prob", type=float, default=DEFAULTS["buy_prob"],
            help="probability of BUY decision (random mode only)")
    group.add_argument("--buy-prob-auto", action="store_true",
            help="set BUY probability from training period")
    group.add_argument("--oracle", dest="mode", action="store_const", const="oracle",
            help="simulate trading always taking the right decision")
    group.add_argument("--oracle-prob", type=float, default=DEFAULTS["oracle_prob"],
            help="probability with which oracle takes right decision")
    group = parser.add_argument_group("output options")
    group.add_argument("-o", "--log-file", metavar="FILE",
            help="name of output file with full trading log")
    group.add_argument("-j", "--json", action="store_true",
            help="only output JSON object with configuration and results")
    group = parser.add_argument_group("execution settings")
    group.add_argument("--gpu-fraction", type=float,
            help="fraction of GPU memory to be allocated")
    group.add_argument("--no-gpu", action="store_true",
            help="prevent from using GPU")
    args = parser.parse_args(args)
    if not args.mode:
        args.mode = "model"
        configure_tensorflow(gpu_memory=args.gpu_fraction, gpu_disable=args.no_gpu)
    config = vars(args)
    results, trace = run(silent=args.json, **config)
    if args.json:
        import json
        print(json.dumps({"config": config, "results": results}))
    else:
        for key, value in results.items():
            print("{:>24}: {:12.3f}".format(key, value))
    if args.log_file:
        trace.to_csv(args.log_file)

if __name__ == "__main__":
    main(*sys.argv[1:])
